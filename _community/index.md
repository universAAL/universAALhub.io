---
layout: default
---

#universAAL Community 

{% for doc in site.collections.community.doc s%}{% if doc.in_menu %}
* [{{ doc.title }}]({{ doc.url | prepend: site.baseurl}})
{% endif %}{% endfor %}
