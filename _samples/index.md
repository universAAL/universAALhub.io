---
layout: default
---

#Examples

{% for doc in site.collections.samples.docs%}{% if doc.in_menu %}
* [{{ doc.title }}]({{ doc.url | prepend: site.baseurl}})
{% endif %}{% endfor %}